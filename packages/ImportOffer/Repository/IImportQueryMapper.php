<?php

namespace ImportOffer\Repository;

use Core\DB\IDBConnection;
use ImportOffer\ImportCollection;

/**
 * Interface IImportQueryMapper
 * @package ImportOffer\Repository
 */
interface IImportQueryMapper
{
    /**
     * @param SearchImportRules $rules
     * @param IDBConnection $dbConnection
     * @return string
     */
    public function getImportsQuery(SearchImportRules $rules, IDBConnection $dbConnection): string;

    /**
     * @param ImportCollection $importCollection
     * @param IDBConnection $dbConnection
     * @return string
     */
    public function saveImportsQuery(ImportCollection $importCollection, IDBConnection $dbConnection): string;

    /**
     * @return string
     */
    public function getImportsCountQuery(): string ;
}
<?php

namespace ImportOffer\Repository;

use Core\DB\QueryRule\BaseQueryRule;

/**
 * Class searchImportRules
 * @package ImportOffer\Repository
 */
class SearchImportRules extends BaseQueryRule
{
    /**
     * @var string
     */
    public $nameMask;

    /**
     * @var integer
     */
    public $code;

    /**
     * @var string
     */
    public $conditionConnection = 'AND';

    /**
     * @return string
     */
    public function getSerializedRule(): string
    {
        $queryRules = [];
        if (!empty($this->nameMask)) {
            $queryRules[] = sprintf('name LIKE \'%%%s%%\'', $this->nameMask);
        }

        if ($this->code > 0) {
            $queryRules[] = sprintf('code = %d', $this->code);
        }

        return implode(' '.$this->conditionConnection. ' ', $queryRules) . ' ' . parent::getSerializedRule();
    }

    /**
     * @return bool
     */
    public function isSearchConditionSet(): bool
    {
        return !empty($this->nameMask) || $this->code > 0;
    }


}
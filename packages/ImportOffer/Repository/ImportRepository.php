<?php

namespace ImportOffer\Repository;

use Core\DB\IDBConnection;
use ImportOffer\Import;
use ImportOffer\ImportCollection;
use ImportOffer\Offer;

/**
 * Class ImportRepository
 * @package ImportOffer\Repository
 */
class ImportRepository implements IImportRepository
{
    /**
     * @var IDBConnection
     */
    private $dbConnection;

    /**
     * @var IImportQueryMapper
     */
    private $queryMapper;

    public function __construct(IDBConnection $connection, IImportQueryMapper $queryMapper)
    {
        $this->dbConnection = $connection;
        $this->queryMapper = $queryMapper;
    }

    /**
     * @inheritDoc
     */
    public function saveImports(ImportCollection $importCollection): int
    {
        $this->dbConnection->query(
            $this->queryMapper->saveImportsQuery($importCollection, $this->dbConnection)
        );

        return $this->dbConnection->getRowsAffected();
    }

    /**
     * @inheritDoc
     */
    public function getImports(SearchImportRules $rules): ImportCollection
    {
        $this->dbConnection->query(
            $this->queryMapper->getImportsQuery($rules, $this->dbConnection)
        );
        $collection = new ImportCollection();
        $this->dbConnection->fetchResult(
            static function (array $row) use ($collection) {
                if (isset($row['usage'])) {
                    $row['usage'] = json_decode($row['usage'], true);
                }
                if (isset($row['offers'])) {
                    $row['offers'] = json_decode($row['offers'], true);
                }
                $import = new Import($row);
                foreach ($row['offers'] as $offerData) {
                    $import->addOffer(new Offer($offerData + ['code' => $import->getId()]));
                }
                $collection->add($import);
            }
        );

        return $collection;
    }

    /**
     * @return int
     */
    public function getImportsCount(): int
    {
        $this->dbConnection->query(
            $this->queryMapper->getImportsCountQuery()
        );
        $result = $this->dbConnection->fetchResult();
        return isset($result[0]['total']) ? (int)$result[0]['total'] : 0;
    }
}
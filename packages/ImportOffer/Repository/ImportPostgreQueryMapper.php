<?php

namespace ImportOffer\Repository;

use Core\DB\IDBConnection;
use Core\Validation\BaseValidation;
use ImportOffer\ImportCollection;

/**
 * Class ImportPostgreQueryMapper
 * @package ImportOffer\Repository
 */
class ImportPostgreQueryMapper implements IImportQueryMapper
{
    use BaseValidation;
    /**
     * @var string
     */
    private $schema;

    /**
     * @var string
     */
    private $tableName;

    /**
     * ImportPostgreQueryMapper constructor.
     * @param string $tableName
     * @param string $schema
     */
    public function __construct($tableName = 'import', $schema = 'public')
    {
        $this->validateNonEmptyString($tableName);
        $this->validateNonEmptyString($schema);
        $this->tableName = $tableName;
        $this->schema = $schema;
    }

    /**
     * @inheritDoc
     */
    public function getImportsQuery(SearchImportRules $rules, IDBConnection $dbConnection): string
    {
        $rules->nameMask = $dbConnection->escapeString($rules->nameMask);
        $conditionString = $rules->getSerializedRule();
        if ($rules->isSearchConditionSet()) {
            $conditionString = 'WHERE '.$conditionString;
        }
        return sprintf(
            'SELECT 
            code, name, weight, array_to_json(usage) as usage, array_to_json(offers) as offers
            FROM %s.%s %s ',
          $this->schema,
          $this->tableName,
          $conditionString
        );
    }

    /**
     * @inheritDoc
     */
    public function saveImportsQuery(ImportCollection $importCollection, IDBConnection $dbConnection): string
    {
        if ($importCollection->isEmpty()) {
            throw new \InvalidArgumentException('Пустая коллекция');
        }

        $importValues = [];
        foreach ($importCollection as $import) {
            $offerValues = [];
            if (!$import->getOffers()->isEmpty()) {
                foreach ($import->getOffers() as $offer) {
                    $offerValues[] = sprintf(
                        'ROW(\'%s\', %d, %d)::offer_type',
                        $dbConnection->escapeString($offer->getCity()),
                        $offer->getPrice(),
                        $offer->getQuantity()

                    );
                }
            }
            $importValues[] = sprintf(
                    '(%d, \'%s\', %f, \'{"%s"}\', ARRAY[%s])' ,
                $import->getId(),
                $dbConnection->escapeString($import->getName()),
                $import->getWeight(),
                $dbConnection->escapeString(implode('","', $import->getUsage())),
                implode(',', $offerValues)
            );
        }

        return sprintf(
            'INSERT INTO %s.%s (code, name, weight, usage, offers) VALUES %s 
            ON CONFLICT(code) DO UPDATE 
            SET name = EXCLUDED.name , weight = EXCLUDED.weight, usage = EXCLUDED.usage',
            $this->schema,
            $this->tableName,
            implode(',', $importValues)
        );
    }

    /**
     * @inheritDoc
     */
    public function getImportsCountQuery(): string
    {
        return sprintf('SELECT COUNT(*) as total FROM %s.%s', $this->schema, $this->tableName);
    }
}
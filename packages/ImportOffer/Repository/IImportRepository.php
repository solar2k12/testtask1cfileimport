<?php


namespace ImportOffer\Repository;


use ImportOffer\ImportCollection;

interface IImportRepository
{
    /**
     * @param ImportCollection $importCollection
     * @return int
     */
    public function saveImports(ImportCollection $importCollection): int;

    /**
     * @param SearchImportRules $rules
     * @return ImportCollection
     */
    public function getImports(SearchImportRules $rules): ImportCollection;
}
<?php

namespace ImportOffer;

use Core\Object\Collection;

/**
 * Class OfferCollection
 * @package ImportOffer
 * @method Offer get($id)
 * @method Offer current()
 * @method self next()
 * @method self rewind()
 */
class OfferCollection extends Collection
{
    protected static $collectionClass = Offer::class;

    /**
     * @return array
     */
    public function getCities(): array
    {
        if ($this->isEmpty()) {
            return [];
        }
        return array_map(static function (Offer $offer){
            return $offer->getCity();
        }, $this->objects);
    }

    /**
     * @param array $citiesFilter
     * @return self
     */
    public function filterByCities(array $citiesFilter = []): self
    {
        if ($this->isEmpty()) {
            return new self([]);
        }
        return new self(
            array_filter($this->objects, static function (Offer $offer) use ($citiesFilter){
                return empty($citiesFilter) || in_array($offer->getCity(), $citiesFilter, true);
            })
        );
    }
}
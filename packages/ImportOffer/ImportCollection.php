<?php

namespace ImportOffer;

use Core\Object\Collection;

/**
 * Коллекция из объектов представления товаров
 * Class ImportCollection
 * @package ImportOffer
 * @method Import get($id)
 * @method Import current()
 * @method self next()
 * @method self rewind()
 */
class ImportCollection extends Collection
{
    protected static $collectionClass = Import::class;
}
<?php

namespace ImportOffer;

use Core\Object\IIdObject;
use Core\Validation\BaseValidation;

class Import implements IIdObject
{
    use BaseValidation;

    /**
     * @var integer
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $weight;

    /**
     * @var array
     */
    private $usage;

    /**
     * @var OfferCollection
     */
    private $offers;

    /**
     * Import constructor.
     * @param array $entityData
     */
    public function __construct(array $entityData)
    {
        $this->code = $entityData['code'] ?? null;
        $this->name = $entityData['name'] ?? null;
        $this->weight = $entityData['weight'] ?? null;
        $this->usage = $entityData['usage'] ?? [];
        $this->validateNonEmptyString($this->name);
        $this->validatePositiveIntegerValue($this->code, false);
        $this->validatePositiveNumericValue($this->weight);
        $this->offers = new OfferCollection();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @return array
     */
    public function getUsage(): array
    {
        return $this->usage;
    }

    /**
     * @return OfferCollection
     */
    public function getOffers(): OfferCollection
    {
        return $this->offers;
    }

    /**
     * @param Offer $offer
     */
    public function addOffer(Offer $offer): void
    {
        $this->offers->add($offer);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->code;
    }

    public function normalize()
    {
        return [
            'code' => $this->code,
            'name' => $this->name,
            'weight' => $this->weight,
            'usage' => $this->usage,
            'offers' => array_values(
                $this->offers->map(static function (Offer $offer) {return $offer->normalize(false);})
            )
        ];
    }
}
<?php

namespace ImportOffer;

use Core\Object\IIdObject;
use Core\Validation\BaseValidation;

class Offer implements IIdObject
{
    use BaseValidation;

    /**
     * @var string
     */
    private $city;

    /**
     * @var integer
     */
    private $code;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var array
     */
    private $prices;

    /**
     * OfferEntity constructor.
     * @param array $entityData
     */
    public function __construct(array $entityData)
    {
        $this->code = $entityData['code'] ?? null;
        $this->prices = $entityData['prices'] ?? [0];
        $this->city = $entityData['city'] ?? null;
        $this->quantity = $entityData['quantity'] ?? 0;
        $this->validatePositiveIntegerValue($this->code, false);
        $this->validateNonEmptyArray($this->prices);
        $this->validateNonEmptyString($this->city);
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return array
     */
    public function getPrices(): array
    {
        return $this->prices;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return reset($this->prices);
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return sprintf('%s-%d', $this->city, $this->code);
    }

    /**
     * @param bool $includeCode
     * @return array
     */
    public function normalize($includeCode = true): array
    {
        $normalized = [
            'city' => $this->city,
            'price' => $this->getPrice(),
            'quantity' => $this->quantity
        ];
        if ($includeCode) {
            $normalized['code'] = $this->code;
        }

        return $normalized;
    }
}

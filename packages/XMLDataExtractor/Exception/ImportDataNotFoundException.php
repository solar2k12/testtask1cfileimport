<?php

namespace XMLDataExtractor\Exception;

use RuntimeException;

/**
 * Исключение для случая, когда данные или файлы не найдены
 * Class ImportDataNotFoundException
 */
class ImportDataNotFoundException extends RuntimeException {}
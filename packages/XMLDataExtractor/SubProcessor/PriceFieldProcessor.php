<?php

namespace XMLDataExtractor\SubProcessor;

use Core\Object\IIdObject;
use Core\Validation\BaseValidation;
use SimpleXMLElement;
use XMLDataExtractor\ISubProcessor;

class PriceFieldProcessor implements ISubProcessor, IIdObject
{
    use BaseValidation;

    /**
     * @var string
     */
    private $name;

    /**
     * PriceFieldProcessor constructor.
     * @param string $name
     */
    public function __construct($name = 'price')
    {
        $this->validateNonEmptyString($name);
        $this->name = $name;
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
       return $this->name;
    }

    /**
     * @param SimpleXMLElement $root
     * @param array $rules
     * @return int|mixed|string
     */
    public function run(SimpleXMLElement $root, array $rules = [])
    {
        $price = 0;
        if ($root->count()) {
            foreach ($root->children() as $child) {
                if (isset($rules[$child->getName()])) {
                    $price = (string) $child;
                    if (isset($rules[$child->getName()]['type'])) {
                        settype($price, $rules[$child->getName()]['type']);
                        return $price;
                    }
                }
            }
        }

        return $price;

    }
}
<?php

namespace XMLDataExtractor\SubProcessor;

use Core\Object\IIdObject;
use Core\Validation\BaseValidation;
use SimpleXMLElement;
use XMLDataExtractor\ISubProcessor;

/**
 * Class ClassCity
 * @package XMLProcessor\SubProcessor
 */
class CityFieldProcessor implements ISubProcessor, IIdObject
{
    use BaseValidation;

    /**
     * @var string
     */
    private $cityPattern = '/^Классификатор\s+\(\s*(?P<city>.*)\s*\)$/u';
    /**
     * @var string
     */
    private $name;

    /**
     * City constructor.
     * @param string $name
     * @param null $cityPattern
     */
    public function __construct($name = 'city', $cityPattern = null)
    {
        $this->validateNonEmptyString($name);
        $this->name = $name;
        if ($cityPattern) {
            $this->validateNonEmptyString($cityPattern);
            $this->cityPattern = $cityPattern;
        }
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->name;
    }

    /**
     * @param SimpleXMLElement $root
     * @param array $rules
     * @return string | null
     */
    public function run(SimpleXMLElement $root, array $rules = []): ?string
    {
        if (preg_match($this->cityPattern, (string)$root, $matches)) {
             return $matches['city'] ?? null;
         }

         return null;
    }
}
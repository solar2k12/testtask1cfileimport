<?php

namespace XMLDataExtractor\SubProcessor;

use Core\Object\IIdObject;
use Core\Validation\BaseValidation;
use SimpleXMLElement;
use XMLDataExtractor\ISubProcessor;

class UsageStructProcessor implements ISubProcessor, IIdObject
{
    use BaseValidation;
    /**
     * @var string
     */
    private $name;

    /**
     * Usage constructor.
     * @param string $name
     */
    public function __construct($name = 'usage')
    {
        $this->validateNonEmptyString($name);
        $this->name = $name;
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->name;
    }

    /**
     * @param SimpleXMLElement $root
     * @param array $rules
     * @return string
     */
    public function run(SimpleXMLElement $root, array $rules = []): string
    {
        $value = implode('-', array_keys($rules));
        foreach ($root->children() as $child) {
            if (isset($rules[$child->getName()])) {
                $value = str_replace($child->getName(), (string) $child, $value);
            }
        }
        return $value;
    }
}
<?php


namespace XMLDataExtractor;

use Core\Object\ImmutableCollection;

/**
 * Class SubProcessorsCollection
 * @package XMLProcessor
 * @method ISubProcessor get($id)
 * @method ISubProcessor current()
 * @method SubProcessorsCollection next()
 * @method SubProcessorsCollection rewind()
 */
class SubProcessorsCollection extends ImmutableCollection
{
    protected static $collectionClass = ISubProcessor::class;
}
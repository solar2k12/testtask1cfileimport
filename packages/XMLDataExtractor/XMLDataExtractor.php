<?php

namespace XMLDataExtractor;

use Core\Validation\BaseValidation;
use SimpleXMLElement;

class XMLDataExtractor
{
    use BaseValidation;
    /**
     * @var array
     */
    private $pathRules;

    /**
     * @var SubProcessorsCollection
     */
    private $subProcessors;

    /**
     * ImportProcessor constructor.
     * @param array $pathRules
     * @param SubProcessorsCollection $subProcessors
     */
    public function __construct(array $pathRules, SubProcessorsCollection $subProcessors)
    {
        $this->validateNonEmptyArray($pathRules);
        $this->pathRules = $pathRules;
        $this->subProcessors = $subProcessors;
    }

    /**
     * @param SimpleXMLElement $root
     * @return array
     */
    public function run(SimpleXMLElement $root): array
    {
        return $this->extractData($root, $this->pathRules);
    }

    /**
     * @param SimpleXMLElement $root
     * @param array $rules
     * @return array | string
     */
    private function extractData(SimpleXMLElement $root, array $rules = [])
    {
        $tag = $root->getName();
        $fields = [];
        if ($tag && isset($rules[$tag])) {
            //правила для аттрибутов тэга
            $attributes = $rules[$tag]['attributes'] ?? null;
            //правила для дочерних тэгов
            $childrenRules = $rules[$tag]['children'] ?? null;
            //обработка дочерних тэгов и внутренних данных через указанный обработчик в subProcessors
            $processor = isset($rules[$tag]['processor']) ? $this->subProcessors->get($rules[$tag]['processor']) : null;
            // правила для тэгов c числами или строками(просто сохранить в указанное поле dataField)
            $dataField = $rules[$tag]['dataField'] ?? null;
            // устновить тип для поля в dataField
            $type = $rules[$tag]['type'] ?? null;
            // сохранить дочерние элементы в массив с названием list
            $list = $rules[$tag]['list'] ?? null;
            // при наличии правила list использует указанное поле как ключ при добавлении в массив
            $useAsKey = $rules[$tag]['useAsKey'] ?? null;

            // Простой тэг с данными типа строка или число
            if ($dataField) {
                $fields[$dataField] = $processor ?
                    $processor->run($root, $childrenRules ?? []) : (string) $root;
                if ($type) {
                    settype($fields[$dataField], $type);
                }
            //Тэг со структурой или необходимой обработкой
            } else if ($processor) {
                $fields = $processor->run($root, $childrenRules);
            //Обработка дочерних тэгов
            } elseif ($childrenRules && $root->count()) {
                foreach ($root->children() as $child) {
                    if (isset($childrenRules[$child->getName()])) {
                        $childData = $this->extractData($child, $childrenRules);
                        // Дочерние тэги в виде массива
                        if ($list) {
                            if (!isset($fields[$list])) {
                                $fields[$list] = [];
                            }

                            if ($useAsKey && isset($childData[$useAsKey])) {
                                $fields[$list][$childData[$useAsKey]] = $childData;
                            } else {
                                $fields[$list][] = $childData;
                            }
                        } else {
                            $fields += $childData;
                        }
                    }
                }

            }
        }

        return $fields;
    }
}
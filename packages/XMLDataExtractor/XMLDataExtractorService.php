<?php


namespace XMLDataExtractor;


use Core\Directory\DirectoryScan;
use Core\Directory\ScanRulesCollection;
use ImportOffer\Import;
use ImportOffer\ImportCollection;
use ImportOffer\Offer;
use XMLDataExtractor\Exception\ImportDataNotFoundException;

class XMLDataExtractorService
{
    /**
     * @var DirectoryScan
     */
    private $directoryScan;

    /**
     * @var XMLDataExtractor
     */
    private $importXMLDataExtractor;

    /**
     * @var XMLDataExtractor
     */
    private $offerXMLDataExtractor;

    /**
     * XMLDataExtractorService constructor.
     * @param DirectoryScan $directoryScan
     * @param $importXMLDataExtractor
     * @param $offerXMLDataExtractor
     */
    public function __construct(
        DirectoryScan $directoryScan,
        $importXMLDataExtractor,
        $offerXMLDataExtractor
    )
    {
        $this->directoryScan = $directoryScan;
        $this->importXMLDataExtractor = $importXMLDataExtractor;
        $this->offerXMLDataExtractor = $offerXMLDataExtractor;
    }

    /**
     * @param $directory
     * @param ScanRulesCollection $scanRules
     * @return ImportCollection
     */
    public function extract($directory, ScanRulesCollection $scanRules): ImportCollection
    {
        $files = $this->directoryScan->scan($directory, $scanRules);
        $importFiles = $files['imports'] ?? [];
        $offerFiles = $files['offers'] ?? [];
        if (count($importFiles) === 0) {
            throw new ImportDataNotFoundException('Файлы с товарами не обнаружены');
        }
        $importCollection = $this->extractDataFromImportFiles($importFiles);
        $this->extractDataFromOfferFiles($offerFiles, $importCollection);

        return $importCollection;
    }

    /**
     * @param array $imports
     * @return ImportCollection
     */
    private function extractDataFromImportFiles(array $imports): ImportCollection
    {
        $importsCollection = new ImportCollection();
        foreach ($imports as $importFile) {
            $importXML = simplexml_load_string(file_get_contents($importFile));
            $importResult = $this->importXMLDataExtractor->run($importXML);
            foreach ($importResult['goods'] as $code => $offer) {
                $importsCollection->add(new Import($offer));
            }
        }

        return $importsCollection;
    }

    /**
     * @param array $offers
     * @param ImportCollection $importCollection
     */
    private function extractDataFromOfferFiles(array $offers, ImportCollection $importCollection): void
    {
        foreach ($offers as $offerFile) {
            $offerXML = simplexml_load_string(file_get_contents($offerFile));
            $offerResult = $this->offerXMLDataExtractor->run($offerXML);
            foreach ($offerResult['offers'] as $code => $offer) {
                if ($importCollection->get($code)) {
                    $importCollection->get($code)->addOffer(
                        new Offer($offer + ['city' => $offerResult['city']])
                    );
                }
            }
        }
    }

}
<?php

namespace XMLDataExtractor;

use SimpleXMLElement;

interface ISubProcessor
{
    /**
     * @param SimpleXMLElement $root
     * @param array $rules
     * @return mixed
     */
    public function run(SimpleXMLElement $root, array $rules = []);
}
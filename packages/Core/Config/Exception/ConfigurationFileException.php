<?php

namespace Core\Config\Exception;

use RuntimeException;

/**
 * Исключение при отсутствии конфигурационного файла или отсутствии доступа на чтение
 * Class ConfigurationFileException
 * @package Core\Config\Exception
 */
class ConfigurationFileException extends RuntimeException {}
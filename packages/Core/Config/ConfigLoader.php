<?php

namespace Core\Config;

use Core\Config\Exception\ConfigurationFileException;
use Core\Environment\EnvironmentType;
use Core\Validation\FileValidation;
use InvalidArgumentException;

/**
 * Class ConfigLoader
 * @package Core\Config
 */
class ConfigLoader
{
    use FileValidation;

    /**
     * @var string
     */
    private $configFile;

    /**
     * @var array
     */
    private $configuarion;

    /**
     * ConfigLoader constructor.
     * @param $configName
     * @param EnvironmentType $currentEnv
     * @param string $defaultConfigDirectory
     */
    public function __construct($configName, EnvironmentType $currentEnv, $defaultConfigDirectory = CONFIG_DIR)
    {
        $this->validateNonEmptyString($configName);
        $this->configFile = sprintf(
            '%s%s%s%s%s.php',
            $defaultConfigDirectory ,
            DIRECTORY_SEPARATOR ,
            $currentEnv->getValue() ,
            DIRECTORY_SEPARATOR ,
            $configName
        );

        if (!$this->isFile($this->configFile) || !$this->isReadable($this->configFile)) {
            throw new ConfigurationFileException(
                sprintf('Файл "%s" не найден или не доступен для чтения', $this->configFile)
            );
        }

        $this->configuarion = include $this->configFile;
    }


    /**
     * @return array
     */
    public function getConfig(): array
    {
        return $this->configuarion;
    }

    /**
     * @param string $field
     * @return mixed
     */
    public function getField($field)
    {
        $this->validateNonEmptyString($field);
        $fields = explode('.', $field);
        if (count($fields) === 1) {
            return $this->configuarion[$field] ?? null;
        }

        $parent = $this->configuarion;
        foreach ($fields as $f) {
            if (!isset($parent[$f])) {
                throw new InvalidArgumentException(
                    sprintf(
                        'Поле "%s" не содержится в конфигурационно файле "%s"',
                        $field,
                        $this->configFile
                    )
                );
            }
            $parent = $parent[$f];
        }

        return $parent;
    }
}
<?php

namespace Core\Environment;

use Core\Object\EnumObject;

/**
 * Class EnvironmentType
 * @package Core\Config
 */
class EnvironmentType extends EnumObject
{
    // локальное окружение
    public const LOCAL = 'local';
    // окружение для разработки
    public const DEVEL = 'devel';
    // боевое окружение
    public const PROD = 'prod';

    /**
     * @var array
     */
    protected static $availableValues = [
        self::LOCAL,
        self::DEVEL,
        self::PROD
    ];

    /**
     * @return EnvironmentType
     */
    public static function getLocal(): EnvironmentType
    {
        return new self(self::LOCAL);
    }

    /**
     * @return EnvironmentType
     */
    public static function getDevel(): EnvironmentType
    {
        return new self(self::DEVEL);
    }

    /**
     * @return EnvironmentType
     */
    public static function getProd(): EnvironmentType
    {
        return new self(self::PROD);
    }

}
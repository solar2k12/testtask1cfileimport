<?php

namespace Core\Environment;

/**
 * Class EnvironmentVariable
 * @package Core\Environment
 */
class EnvironmentVariable
{
    public const PROJECT_ENV = 'PROJECT_ENV';

    /**
     * @return EnvironmentType
     */
    public static function getCurrentEnvSetting()
    {
        if (isset($_ENV[self::PROJECT_ENV]) && EnvironmentType::isValidValue($_ENV[self::PROJECT_ENV])) {
            return new EnvironmentType($_ENV[self::PROJECT_ENV]);
        }

        return EnvironmentType::getLocal();
    }
}
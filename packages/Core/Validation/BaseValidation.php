<?php

namespace Core\Validation;

use \InvalidArgumentException;

/**
 * Трейт для валиадации переменных базового типа
 * Trait BaseValidation
 * @package Core\Validation
 */
trait BaseValidation
{
    /**
     * Валидация положительного числа
     * @param int | float | string $number - валидирумое число
     * @param bool $zeroIncluded - флаг для учета нуля как валидного значения
     * @throws InvalidArgumentException
     */
    protected function validatePositiveNumericValue($number, $zeroIncluded = true): void
    {
        if (!is_numeric($number) || ($zeroIncluded && $number < 0) || (!$zeroIncluded && $number <= 0)) {
            throw new InvalidArgumentException('Значение не является положительным числом');
        }
    }

    /**
     * Валидация положительного целого числа
     * @param int | string $number - валидирумое число
     * @param bool $zeroIncluded - флаг для учета нуля как валидного значения
     * @throws InvalidArgumentException
     */
    protected function validatePositiveIntegerValue($number, $zeroIncluded = true): void
    {
        if (!is_numeric($number) || ($zeroIncluded && $number < 0) || (!$zeroIncluded && $number <= 0)) {
            throw new InvalidArgumentException('Значение не является положительным числом');
        }
    }

    /**
     * Валидация положительного числа с плавающей точкой
     * @param float $number - валидирумое число
     * @param bool $zeroIncluded - флаг для учета нуля как валидного значения
     * @throws InvalidArgumentException
     */
    protected function validatePositiveFloatValue($number, $zeroIncluded = true): void
    {
        if (!is_float($number) || ($zeroIncluded && $number < 0.00) || (!$zeroIncluded && $number <= 0.00)) {
            throw new InvalidArgumentException('Значение не является положительным числом');
        }
    }

    /**
     * Валидация не пустой строки
     * @param string $stringValue
     * @throws InvalidArgumentException
     */
    protected function validateNonEmptyString($stringValue): void
    {
        if (!is_string($stringValue) || $stringValue === '') {
            throw new InvalidArgumentException('Значение не является не пустой строкой');
        }
    }

    /**
     * Валидация массива со значениями
     * @param array $testArray
     * @throws InvalidArgumentException
     */
    protected function validateNonEmptyArray(array $testArray): void
    {
        if (!is_array($testArray) || empty($testArray)) {
            throw new InvalidArgumentException('Переменная не является массивом со значениями');
        }
    }

    /**
     * @param $value
     * @param $type
     * @return bool
     */
    protected function validateType($value, $type): bool
    {
        switch ($type) {
            case 'int':
            case 'integer':
                return is_int($value);
            case 'bool':
            case 'boolean':
                return is_bool($value);
            case 'string':
                return is_string($value);
            case 'float':
                return is_float($value);
            default:
                return false;
        }
    }
}
<?php

namespace Core\Validation;

/**
 * Валидация файлов и директорий
 * Trait FileValidation
 * @package Core\Validation
 */
trait FileValidation
{
    use BaseValidation;

    /**
     * Валидация, что объект под названием $fileName является файлом
     * @param string $fileName
     * @return bool
     */
    protected function isFile($fileName): bool
    {
        $this->validateNonEmptyString($fileName);
        return is_file($fileName);
    }

    /**
     * Валидация, что объект под названием $directoryName является директорией
     * @param string $directoryName
     * @return bool
     */
    protected function isDirectory($directoryName): bool
    {
        $this->validateNonEmptyString($directoryName);
        return is_dir($directoryName);
    }

     /**
     * Валидация, что объект под названием $name доступен для чтения
     * @param string $name
     * @return bool
     */
    protected function isReadable($name): bool
    {
        $this->validateNonEmptyString($name);
        return is_readable($name);
    }
}
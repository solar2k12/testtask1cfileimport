<?php

namespace Core\Object;

/**
 * Интерфейс для объектов с идентификатором
 * Interface IIdObject
 * @package Core\Object
 */
interface IIdObject
{
    /**
     * Возвращает идентификатор объекта
     * @return int | string
     */
    public function getId();
}
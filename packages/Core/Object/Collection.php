<?php


namespace Core\Object;

/**
 * Class Collection
 * @package Core\Object
 */
abstract class Collection extends ImmutableCollection
{
    /**
     * Collection constructor.
     * @param IIdObject[] $objects
     */
    public function __construct(array $objects = [])
    {
        if (!empty($objects)) {
            parent::__construct($objects);
        } else {
            $this->objects = [];
        }
    }

    /**
     * @param IIdObject $object
     * @param bool $overwrite
     * @return bool
     */
    public function add(IIdObject $object, $overwrite = false): bool
    {
        if (!$overwrite) {
            if (!$this->get($object->getId())) {
                $this->objects[$object->getId()] = $object;
            } else {
                return  false;
            }
        } else {
            $this->objects[$object->getId()] = $object;
        }

        return true;
    }
}
<?php

namespace Core\Directory;

use Core\Object\IIdObject;
use Core\Validation\FileValidation;

class ScanRule implements IIdObject
{
    use FileValidation;

    /**
     * @var string
     */
    private $ruleName;

    /**
     * @var string
     */
    private $mimeType;

    /**
     * @var DirectoryScanType
     */
    private $scanType;

    /**
     * @var string
     */
    private $namePattern;

    /**
     * @var bool
     */
    private $isObjectReadable;

    /**
     * ScanRule constructor.
     * @param string $ruleName
     * @param DirectoryScanType $scanType
     * @param $mimeType
     * @param $namePattern
     * @param bool $isReadable
     */
    public function __construct(
        $ruleName,
        DirectoryScanType $scanType,
        $mimeType = '',
        $namePattern = '',
        $isReadable = true
    )
    {
        $this->validateNonEmptyString($ruleName);
        $this->ruleName = $ruleName;
        $this->scanType = $scanType;
        $this->mimeType = $mimeType;
        $this->namePattern = $namePattern;
        $this->isObjectReadable = $isReadable;
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->ruleName;
    }

    /**
     * @param string $objectFullName
     * @return bool
     */
    public function validate($objectFullName): bool
    {
        $objectName = basename($objectFullName);
        return !(empty($objectFullName)
            ||
            ($this->isObjectReadable && !$this->isReadable($objectFullName))
            ||
            (!empty($this->namePattern) && !preg_match($this->namePattern, $objectName))
            ||
            (!empty($this->mimeType) && mime_content_type($objectFullName) !== $this->mimeType)
            ||
            ($this->scanType->isFilesOnly() && !$this->isFile($objectFullName))
            ||
            ($this->scanType->isDirsOnly() && !$this->isDirectory($objectFullName)));
    }
}
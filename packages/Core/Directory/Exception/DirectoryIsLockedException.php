<?php

namespace Core\Directory\Exception;

use RuntimeException;

/**
 * Исключение в случае отсутствия возможности получить файловый дескриптор для чтения директории
 * Class DirectoryIsLockedException
 * @package Core\Directory\Exception
 */
class DirectoryIsLockedException extends RuntimeException {}
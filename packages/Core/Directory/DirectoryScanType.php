<?php

namespace Core\Directory;

use Core\Object\EnumObject;

/**
 * Доступные типы объектов для сканирования директории
 * Class DirectoryScanType
 * @package Core\Directory
 */
class DirectoryScanType extends EnumObject
{
    // Поиск директорий
    public const DIRS = 'dir';
    // Поиск файлов
    public const FILES = 'file';
    // Искать все
    public const ALL = 'all';

    protected static $availableValues = [
        self::DIRS,
        self::FILES,
        self::ALL
    ];

    /**
     * @return bool
     */
    public function isFilesOnly(): bool
    {
        return $this->value === self::FILES;
    }

    /**
     * @return bool
     */
    public function isDirsOnly(): bool
    {
        return $this->value === self::DIRS;
    }

    /**
     * @return DirectoryScanType
     */
    public static function getDirsOnlyRule(): DirectoryScanType
    {
        return new self(self::DIRS);
    }

    /**
     * @return DirectoryScanType
     */
    public static function getFilesOnlyRule(): DirectoryScanType
    {
        return new self(self::FILES);
    }

    /**
     * @return DirectoryScanType
     */
    public static function getAllRules(): DirectoryScanType
    {
        return new self(self::ALL);
    }
}
<?php

namespace Core\Directory;

use Core\Object\ImmutableCollection;

/**
 * Class ScanRulesCollection
 * @package Core\Directory
 * @method ScanRule get($id)
 * @method ScanRule current()
 * @method ScanRulesCollection next()
 * @method ScanRulesCollection rewind()
 */
class ScanRulesCollection extends ImmutableCollection
{
    protected static $collectionClass = ScanRule::class;

    /**
     * @return bool
     */
    public function isMoreThanOne(): bool
    {
        return $this->count() > 1;
    }

    /**
     * @param string $objectFullName
     * @return string
     */
    public function isAcceptable($objectFullName): string
    {
        /** @var ScanRule $rule */
        foreach ($this->objects as $rule) {
            if ($rule->validate($objectFullName)) {
                return $rule->getId();
            }
        }

        return '';
    }
}
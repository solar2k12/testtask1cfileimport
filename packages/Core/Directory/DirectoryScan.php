<?php

namespace Core\Directory;

use Core\Directory\Exception\DirectoryIsLockedException;
use Core\Validation\FileValidation;

/**
 * Класс для поиска объектов в директории
 * Class DirectoryScan
 * @package Core\Directory
 */
class DirectoryScan
{
    use FileValidation;

    // Обозначение текущей директории
    public const CURRENT_DIR = '.';

    // Обозначение родительской директории
    public const PARENT_DIR = '..';

    /**
     * Лимит на количество объектов для поиска
     * @var int
     */
    private $maxScanObjects;

    /**
     * DirectoryScan constructor.
     * @param int $maxScanObjects
     */
    public function __construct($maxScanObjects = 1000)
    {
        $this->validatePositiveIntegerValue($maxScanObjects, false);
        $this->maxScanObjects = $maxScanObjects;
    }

    /**
     * Производит поиск объектов в директории по заданным критериям
     * @param $directory - имя директории для описка
     * @param ScanRulesCollection $rules
     * @return array
     */
    public function scan($directory, ScanRulesCollection $rules): array
    {
        $directory = realpath($directory);
        if (!$this->isDirectory($directory) || !$this->isReadable($directory) || !($handle = opendir($directory))) {
            throw new DirectoryIsLockedException(
                sprintf(
                    'Не удалось получить фаловый дескриптор для открытия директории с именем %s',
                    $directory
                )
            );
        }

        $objectsCount = 0;
        $result = [];
        $isMoreThanOneRule = $rules->isMoreThanOne();
        while (false !== ($objectName = readdir($handle)) && $objectsCount <= $this->maxScanObjects) {
            $fullName = $directory . DIRECTORY_SEPARATOR . $objectName;
            if (
                $objectName === self::CURRENT_DIR
                ||
                $objectName === self::PARENT_DIR
                ||
                (!($ruleName = $rules->isAcceptable($fullName)))
            ) {
                continue;
            }

            if ($isMoreThanOneRule) {
                $result[$ruleName][] = $fullName;
            } else {
                $result[] = $fullName;
            }
            $objectsCount++;
        }
        closedir($handle);

        return  $result;
    }
}
<?php

namespace Core\DB;

/**
 * Interface IDBConnection
 * @package Core\DB
 */
interface IDBConnection
{
    /**
     * @param string $connectionString
     * @return resource
     */
    public function connect($connectionString);

    /**
     * @return bool
     */
    public function isConnectionUp(): bool ;

    /**
     * @param string $queryString
     * @return resource
     */
    public function query($queryString);

    /**
     * @return string
     */
    public function getLastError(): string;

    /**
     * @param string $text
     * @return string
     */
    public function escapeString($text): string;

    /**
     * @return void
     */
    public function close(): void;

    /**
     * @return int
     */
    public function getRowsAffected():int;

    /**
     * @param callable $rowConverter
     * @return mixed
     */
    public function fetchResult(callable $rowConverter = null);
}
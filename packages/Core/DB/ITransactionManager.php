<?php


namespace Core\DB;


interface ITransactionManager
{
    public function begin();

    public function rollback();

    public function commit();

    public function status();
}
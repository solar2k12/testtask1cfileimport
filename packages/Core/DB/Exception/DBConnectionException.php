<?php

/**
 * Исключение при отсутствии соединения с базой данных
 * Class DBConnectionException
 */
class DBConnectionException extends RuntimeException {}
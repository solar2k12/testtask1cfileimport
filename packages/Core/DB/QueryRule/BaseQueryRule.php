<?php

namespace Core\DB\QueryRule;

/**
 * Class BaseQueryRule
 * @package Core\DB\QueryRule
 */
class BaseQueryRule
{
    /**
     * @var integer
     */
    public $limit;

    /**
     * @var integer
     */
    public $offset;

    /**
     * @var string
     */
    public $orderBy;

    /**
     * @var string
     */
    public $orderDirection = 'DESC';

    /**
     * @return string
     */
    public function getSerializedRule(): string
    {
        $queryRules = [];
        if ($this->orderBy) {
            $queryRules[] = sprintf('ORDER BY %s %s', $this->orderBy, $this->orderDirection);
        }

        if ($this->offset > 0) {
            $queryRules[] = sprintf('OFFSET %d', $this->offset);
        }

        if ($this->limit > 0) {
            $queryRules[] = sprintf('LIMIT %d', $this->limit);
        }

        return implode(' ', $queryRules);
    }
}
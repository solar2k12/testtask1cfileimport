<?php

namespace Core\DB;

use Core\Validation\BaseValidation;
use DBConnectionException;

/**
 * Class PostgreDBConnection
 */
class PostgreDBConnection implements IDBConnection
{
    use BaseValidation;

    /**
     * @var self
     */
    private static $instance;

    /**
     * @var resource
     */
    private $connection;

    /**
     * @var int;
     */
    private $affectedRows;

    /**
     * @var resource
     */
    private $result;

    /**
     * PostgreDBConnection constructor.
     */
    private function __construct() {}

    /**
     * @param string $connectionString
     * @return PostgreDBConnection
     */
    public static function getInstance($connectionString = ''): PostgreDBConnection
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
            if (!empty($connectionString)) {
                self::$instance->connect($connectionString);
            }
        }

        return self::$instance;
    }

    /**
     * @inheritDoc
     */
    public function connect($connectionString)
    {
        $this->validateNonEmptyString($connectionString);
        $this->connection = pg_connect($connectionString);
        if ($error = $this->getLastError()) {
            throw new DBConnectionException('Соединение с БД не установлено по причине: ' . $error);
        }

        return $this->connection;
    }

    /**
     * @inheritDoc
     */
    public function query($queryString)
    {
        $this->validateNonEmptyString($queryString);
        $this->validateConnection();

        return $this->result = pg_query($this->connection, $queryString);
    }

    /**
     * @inheritDoc
     */
    public function close(): void
    {
        pg_close($this->connection);
    }

    /**
     * @inheritDoc
     */
    public function isConnectionUp(): bool
    {
        return pg_connection_status($this->connection) === PGSQL_CONNECTION_OK;
    }

    /**
     * @return resource
     * @throws DBConnectionException
     */
    public function validateConnection()
    {
        if (!$this->isConnectionUp()) {
            throw new DBConnectionException('Соединение с БД не установлено');
        }
        return $this->connection;
    }

    /**
     * @inheritDoc
     */
    public function getLastError(): string
    {
        return pg_last_error($this->connection);
    }

    /**
     * @param string $text
     * @return string
     */
    public function escapeString($text): string
    {
        return pg_escape_string($this->connection, $text);
    }

    /**
     * @inheritDoc
     */
    public function getRowsAffected(): int
    {
        return pg_affected_rows($this->result);
    }

    /**
     * @inheritDoc
     */
    public function fetchResult(callable $rowConverter = null)
    {
        if (!$rowConverter) {
            return pg_fetch_all($this->result);
        }
        $converterResult = [];
        while ($row = pg_fetch_array($this->result, null, PGSQL_ASSOC)) {
            $converterResult = $rowConverter($row);
        }

        return $converterResult;
    }
}
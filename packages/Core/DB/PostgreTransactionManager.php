<?php

namespace Core\DB;

use Core\Validation\BaseValidation;

/**
 * Class PostgreTransactionManager
 * @package Core\DB
 */
class PostgreTransactionManager implements ITransactionManager
{
    use BaseValidation;

    /**
     * @var PostgreDBConnection
     */
    private $connection;

    /**
     * @var PostgreTransactionStatus
     */
    private $status;

    /**
     * PostgreTransactionManager constructor.
     * @param PostgreDBConnection $connection
     */
    public function __construct(PostgreDBConnection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return PostgreTransactionStatus
     */
    public function begin()
    {
        return $this->executeTransactionCommand('BEGIN');
    }

    public function rollback()
    {
        return $this->executeTransactionCommand('ROLLBACK');
    }

    public function commit()
    {
        return $this->executeTransactionCommand('COMMIT');
    }

    public function status()
    {
        return $this->status;
    }

    /**
     * @param $command
     * @return PostgreTransactionStatus
     */
    private function executeTransactionCommand($command): PostgreTransactionStatus
    {
        $connectionResource = $this->connection->validateConnection();
        $this->validateNonEmptyString($command);
        $this->connection->query($command);
        $this->status = new PostgreTransactionStatus(pg_transaction_status($connectionResource));

        return $this->status;
    }
}
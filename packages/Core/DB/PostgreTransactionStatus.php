<?php

namespace Core\DB;

use Core\Object\EnumObject;

/**
 * Class PostgreTransactionStatus
 * @package Core\DB
 */
class PostgreTransactionStatus extends EnumObject
{
    /**
     * @var array 
     */
    protected static $availableValues = [
        PGSQL_TRANSACTION_ACTIVE,
        PGSQL_TRANSACTION_INTRANS,
        PGSQL_TRANSACTION_INERROR,
        PGSQL_TRANSACTION_UNKNOWN,
    ];

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->value === PGSQL_TRANSACTION_ACTIVE;
    }
}
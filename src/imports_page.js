import React from "react";
import {render} from "react-dom";

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            pages: null,
            loading: true,
            sorted: []
        };
        this.fetchData = this.fetchData.bind(this);
    }

    fetchData(state, instance) {
        this.setState({loading: true});
        fetch('view_import.php?itemsPerPage=' + state.pageSize + '&page=' + state.page, {
            method: 'GET',
        })
            .then(response => response.json())
            .then(result => {
                this.setState({
                    data: result.rows,
                    pages: result.pages,
                    loading: false
                });
            });
    }

    render() {
        const {data, pages, loading} = this.state;
        let offer_columns = [];
        if (data.length) {
            data[0].offers.forEach((offer, key) => {
                    offer_columns.push({
                            Header: "Цена(" + offer.city + ")",
                            accessor: d => d.offers[key].price,
                            id: "price"+key
                        });
                    offer_columns.push({
                            Header: "Кол-во(" + offer.city + ")",
                            accessor: d => d.offers[key].quantity,
                            id: "quantity"+key
                    });
                }
            );
        }
        const columns = [
            {
                Header: "Товар",
                columns: [
                    {
                        Header: "Код",
                        accessor: "code"
                    },
                    {
                        Header: "Наз-ние",
                        accessor: "name"
                    },
                    {
                        Header: "Вес",
                        accessor: "weight"
                    },
                    {
                        Header: "Аналоги",
                        accessor: "usage",
                        Cell: ({value}) => {
                            return value.join(', ');
                        }
                    }
                ]
            },
            {
                Header: "Предложения",
                columns: offer_columns
            }
        ];
        return (
            <div>
                <ReactTable
                    columns={columns}
                    manual
                    data={data}
                    pages={pages}
                    loading={loading}
                    onFetchData={this.fetchData}
                    pageSize={50}
                />
            </div>
        );
    }
}

render(<App/>, document.getElementById("root"));

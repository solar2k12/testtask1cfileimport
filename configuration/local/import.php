<?php
return [
    'КоммерческаяИнформация' => [
        'children' => [
            'Классификатор' => [
                'children' => [
                    'Наименование' => ['processor' => 'city', 'dataField' => 'city', 'type' => 'string']
                ]
            ],
            'Каталог' => [
                'attributes' => ['СодержитТолькоИзменения' => 'boolean'],
                'children' => [
                    'Товары' => [
                        'list' => 'goods',
                        'useAsKey' => 'code',
                        'children' => [
                            'Товар' => [
                                'children' => [
                                    'Наименование' => ['dataField' => 'name', 'type' => 'string'],
                                    'Код' => ['dataField' => 'code', 'type' => 'int'],
                                    'Вес' => ['dataField' => 'weight', 'type' => 'float'],
                                    'Взаимозаменяемости' => [
                                        'list' => 'usage',
                                        'children' => [
                                            'Взаимозаменяемость' => [
                                                'processor' => 'usage',
                                                'children' => [
                                                    'Марка' => ['type' => 'string'],
                                                    'Модель' => ['type' => 'string'],
                                                    'КатегорияТС' => ['type' => 'string']
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]

                        ]
                    ]
                ]
            ]
        ]
    ]
];
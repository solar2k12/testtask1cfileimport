<?php
return [
    'КоммерческаяИнформация' => [
        'children' => [
            'Классификатор' => [
                'children' => [
                    'Наименование' => ['processor' => 'city', 'dataField' => 'city', 'type' => 'string']
                ]
            ],
            'ПакетПредложений' => [
                'attributes' => ['СодержитТолькоИзменения' => 'boolean'],
                'children' => [
                    'Предложения' => [
                        'list' => 'offers',
                        'useAsKey' => 'code',
                        'children' => [
                            'Предложение' => [
                                'children' => [
                                    'Код' => ['dataField' => 'code', 'type' => 'int'],
                                    'Количество' => ['dataField' => 'quantity', 'type' => 'int'],
                                    'Цены' => [
                                        'list' => 'prices',
                                        'children' => [
                                            'Цена' => [
                                                'processor' => 'price',
                                                'children' => [
                                                    'ЦенаЗаЕдиницу' => ['type' => 'int']
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]
];

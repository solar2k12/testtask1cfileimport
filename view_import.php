<?php
include_once 'init.php';

use Core\Config\ConfigLoader;
use Core\DB\PostgreDBConnection;
use Core\Environment\EnvironmentVariable;
use ImportOffer\Import;
use ImportOffer\Repository\ImportPostgreQueryMapper;
use ImportOffer\Repository\ImportRepository;
use ImportOffer\Repository\SearchImportRules;

$page = $_GET['page'] ?? 0;
$itemsPerPage = $_GET['itemsPerPage'] ?? 5;
$dbConfig = new ConfigLoader('database', EnvironmentVariable::getCurrentEnvSetting());
$connectionString =
    sprintf(
        'host=%s port=%d dbname=%s user=%s password=%s',
        $dbConfig->getField('host'),
        $dbConfig->getField('port'),
        $dbConfig->getField('dbname'),
        $dbConfig->getField('user'),
        $dbConfig->getField('password')
    );
$dbConnection = PostgreDBConnection::getInstance($connectionString);

$repos = new ImportRepository($dbConnection, new ImportPostgreQueryMapper());
$rules = new SearchImportRules();
$rules->orderBy = 'code';
$rules->limit = $itemsPerPage;
$rules->offset = $page * $itemsPerPage;
$imports = $repos->getImports($rules);
$total = $repos->getImportsCount();
$handler = fopen('PHP://OUTPUT', 'wb+');
fwrite(
    $handler,
    json_encode(
        [
            'pages' =>(int)ceil($total / $itemsPerPage),
            'rows' => array_values(
                $imports->map(
                    static function (Import $import) {
                        return $import->normalize();
                    }
                )
            )
        ]
    )
);
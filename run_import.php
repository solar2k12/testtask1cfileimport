<?php
include_once 'init.php';

use Core\Config\ConfigLoader;
use Core\DB\PostgreDBConnection;
use Core\Directory\DirectoryScan;
use Core\Directory\DirectoryScanType;
use Core\Directory\ScanRule;
use Core\Directory\ScanRulesCollection;
use Core\Environment\EnvironmentVariable;
use ImportOffer\Repository\ImportPostgreQueryMapper;
use ImportOffer\Repository\ImportRepository;
use XMLDataExtractor\SubProcessor\PriceFieldProcessor;
use XMLDataExtractor\XMLDataExtractor;
use XMLDataExtractor\SubProcessor\CityFieldProcessor;
use XMLDataExtractor\SubProcessor\UsageStructProcessor;
use XMLDataExtractor\SubProcessorsCollection;
use XMLDataExtractor\XMLDataExtractorService;

$opts = 'd:v';
$options = getopt($opts);
if (empty($options['d'])) {
    echo 'Usage "php run_import.php -d <directory> -v for verbose"';
    exit;
}
$dataDir = $options['d'];
$verbose = isset($options['v']) ?? false;
if ($verbose) {
    echo 'Подготовка...';
}

// Сканирование директории и импорт в промежуточные сущности для дальнейшего импорта в БД
$importFileNamePattern = '/^import\d+\_\d+\.xml$/';
$offerFileNamePattern = '/^offers\d+\_\d+\.xml$/';
$dirScanRules = new ScanRulesCollection([
    new ScanRule(
        'imports',
        DirectoryScanType::getFilesOnlyRule(),
        'text/xml',
        $importFileNamePattern
    ),
    new ScanRule('offers', DirectoryScanType::getFilesOnlyRule(), 'text/xml', $offerFileNamePattern)
]);
$dirScan = new DirectoryScan();
$importRulesConfiguration = new ConfigLoader('import', EnvironmentVariable::getCurrentEnvSetting());
$importExtractor = new XMLDataExtractor($importRulesConfiguration->getConfig(), new SubProcessorsCollection([
    new CityFieldProcessor(), new UsageStructProcessor()
]));
$offerRulesConfiguration = new ConfigLoader('offer', EnvironmentVariable::getCurrentEnvSetting());
$offerExtractor = new XMLDataExtractor($offerRulesConfiguration->getConfig(), new SubProcessorsCollection([
    new CityFieldProcessor(), new PriceFieldProcessor()
]));
$extractorService = new XMLDataExtractorService($dirScan, $importExtractor, $offerExtractor);
if ($verbose) {
    echo 'OK' . PHP_EOL . 'Импорт товаров из файлов...';
}
$importCollection = $extractorService->extract($dataDir, $dirScanRules);
if ($verbose) {
    echo 'Найдено ' . $importCollection->count() . PHP_EOL . 'Загрузка в БД...';
}

// Сохранение в БД
$dbConfig = new ConfigLoader('database', EnvironmentVariable::getCurrentEnvSetting());
$connectionString =
    sprintf(
        'host=%s port=%d dbname=%s user=%s password=%s',
        $dbConfig->getField('host'),
        $dbConfig->getField('port'),
        $dbConfig->getField('dbname'),
        $dbConfig->getField('user'),
        $dbConfig->getField('password')
    );
$dbConnection = PostgreDBConnection::getInstance($connectionString);

$repos = new ImportRepository($dbConnection, new ImportPostgreQueryMapper());
$rowsAffected = $repos->saveImports($importCollection);
if ($verbose) {
    echo $rowsAffected.' записей сохранено'.PHP_EOL;
}
$dbConnection->close();